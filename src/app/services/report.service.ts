import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { StorageService } from "./storage.service";

@Injectable({
  providedIn: "root"
})
export class ReportService {
  private url = environment.API_URL;
  public token: any;

  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) {
    this.getToken();
  }

  getStatementByPeriod(data: any) {
    console.log("report.getStatementByPeriod");
    const headers = new HttpHeaders({
      Authorization: "Bearer " + this.token,
      Accept: "application/json"
    });
    const options = { headers: headers };

    return this.http.post(this.url + "/api/v1/statement", data, options);
  }

  sumChartsByPeriod(data: any) {
    console.log("report.sumChartsByPeriod");
    const headers = new HttpHeaders({
      Authorization: "Bearer " + this.token,
      Accept: "application/json"
    });
    const options = { headers: headers };

    return this.http.post(this.url + "/api/v1/charts", data, options);
  }

  getToken() {
    const token =
      this.storageService.getLocalStorage(environment.AUTH_TOKEN) || null;
    if (token) {
      this.token = token.access_token;
    }
  }
}
