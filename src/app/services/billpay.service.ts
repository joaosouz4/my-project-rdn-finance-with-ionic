import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { StorageService } from "./storage.service";

@Injectable({
  providedIn: "root"
})
export class BillpayService {
  private url = environment.API_URL;
  public token: any;

  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) {
    this.getToken();
  }

  index() {
    console.log("bill_pays.index");
    const headers = new HttpHeaders({
      Authorization: "Bearer " + this.token,
      Accept: "application/json"
    });
    const options = { headers: headers };

    return this.http.get(this.url + "/api/v1/bill_pays", options);
  }

  store(data: any) {
    console.log("bill_pays.store");
    const headers = new HttpHeaders({
      Authorization: "Bearer " + this.token,
      Accept: "application/json"
    });
    const options = { headers: headers };

    return this.http.post(this.url + "/api/v1/bill_pays/store", data, options);
  }

  show(id: Number) {
    console.log("bill_pays.show");
    const headers = new HttpHeaders({
      Authorization: "Bearer " + this.token,
      Accept: "application/json"
    });
    const options = { headers: headers };

    return this.http.get(this.url + "/api/v1/bill_pays/show/" + id, options);
  }

  update(data: any, id: Number) {
    console.log("bill_pays.update");
    const headers = new HttpHeaders({
      Authorization: "Bearer " + this.token,
      Accept: "application/json"
    });
    const options = { headers: headers };

    return this.http.put(
      this.url + "/api/v1/bill_pays/update/" + id,
      data,
      options
    );
  }

  destroy(id: Number) {
    console.log("bill_pays.destroy");
    const headers = new HttpHeaders({
      Authorization: "Bearer " + this.token,
      Accept: "application/json"
    });
    const options = { headers: headers };

    return this.http.delete(
      this.url + "/api/v1/bill_pays/destroy/" + id,
      options
    );
  }

  getToken() {
    const token =
      this.storageService.getLocalStorage(environment.AUTH_TOKEN) || null;
    if (token) {
      this.token = token.access_token;
    }
  }
}
