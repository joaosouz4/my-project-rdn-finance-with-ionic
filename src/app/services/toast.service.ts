import { Injectable } from "@angular/core";
import { ToastController } from "@ionic/angular";

@Injectable({
  providedIn: "root"
})
export class ToastService {
  constructor(public toastController: ToastController) {}

  async showToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
      buttons: ["Close"]
    });
    toast.present();
  }

  async showErrorToast(error: any) {
    if (error.status === 400 || error.status === 401) {
      this.showToast("Dados inválidos, favor, informar os dados de acesso");
    }

    if (error.status === 422) {
      this.showToast("Falha de validação, verifique os campos digitados");
    }

    if (error.status === 404 || error.status === 500) {
      this.showToast(
        "Impossível se conectar ao servidor, verifique sua conexão ou tente novamente em alguns minutos"
      );
    }
  }
}
