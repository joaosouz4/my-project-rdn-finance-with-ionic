import { Component, OnInit } from "@angular/core";
import { CategoryService } from "src/app/services/category.service";
import { ToastService } from "src/app/services/toast.service";
import { AlertController, LoadingController } from "@ionic/angular";

@Component({
  selector: "app-category",
  templateUrl: "./category.page.html",
  styleUrls: ["./category.page.scss"]
})
export class CategoryPage implements OnInit {
  form: any = { name: null };

  categories: any;

  idAlt: Number = null;

  editable: boolean = false;

  constructor(
    public service: CategoryService,
    public toast: ToastService,
    private alertCtrl: AlertController,
    private loading: LoadingController
  ) {}

  ngOnInit() {
    this.index();
  }

  index() {
    this.presentLoading();
    this.service.index().subscribe(
      (response: any) => {
        this.categories = response.data;
        this.closeLoading();
      },
      (err: any) => {
        this.categories = [];
        this.closeLoading();
        this.toast.showErrorToast(err);
      }
    );
  }

  save() {
    if (this.form.name !== null) {
      this.presentLoading();
      this.service.store(this.form).subscribe(
        res => {
          this.closeLoading();
          this.toast.showToast("Categoria adicionada com sucesso");
          this.reset();
          this.editable = false;
          this.index();
        },
        (err: any) => {
          this.closeLoading();
          this.toast.showErrorToast(err);
        }
      );
    }
  }

  edit(id: Number) {
    this.presentLoading();
    this.service.show(id).subscribe(
      (res: any) => {
        this.closeLoading();
        this.idAlt = res.data.id;
        this.form.name = res.data.name;
        this.editable = true;
      },
      (err: any) => {
        this.closeLoading();
        this.editable = false;
        this.toast.showErrorToast(err);
      }
    );
  }

  update() {
    if (this.form.name !== null && this.idAlt !== null) {
      this.presentLoading();
      this.service.update(this.form, this.idAlt).subscribe(
        res => {
          this.closeLoading();
          this.toast.showToast("Categoria atualizada com sucesso");
          this.reset();
          this.editable = false;
          this.index();
        },
        (err: any) => {
          this.closeLoading();
          this.toast.showErrorToast(err);
        }
      );
    }
  }

  del(id: Number) {
    this.presentLoading();
    this.service.destroy(id).subscribe(
      res => {
        this.closeLoading();
        this.toast.showToast("Categoria deletada com sucesso");
        this.reset();
        this.editable = false;
        this.index();
      },
      (err: any) => {
        this.closeLoading();
        this.toast.showErrorToast(err);
      }
    );
  }

  reset() {
    this.form.name = null;
    this.idAlt = null;
  }

  async presentConfirm(id: any) {
    let alert = await this.alertCtrl.create({
      message: "Confirmar a exclusão do item?",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel"
        },
        {
          text: "Confirmar",
          handler: () => {
            this.del(id);
          }
        }
      ]
    });
    alert.present();
  }

  async presentLoading() {
    const loading = await this.loading.create({
      message: "Aguarde..."
    });
    await loading.present();
  }

  async closeLoading() {
    await this.loading.dismiss();
  }
}
