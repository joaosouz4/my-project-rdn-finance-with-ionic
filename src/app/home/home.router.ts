import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HomePage } from "./home.page";
import { HomeGuard } from "../guards/home.guard";
import { UserResolver } from "../resolvers/user.resolver";

const routes: Routes = [
  {
    path: "home",
    component: HomePage,
    canActivate: [HomeGuard],
    resolve: {
      userData: UserResolver
    },
    children: [
      {
        path: "dashboard",
        loadChildren: () =>
          import("../pages/dashboard/dashboard.module").then(
            m => m.DashboardPageModule
          )
      },
      {
        path: "category",
        loadChildren: () =>
          import("../pages/category/category.module").then(
            m => m.CategoryPageModule
          )
      },
      {
        path: "billpay",
        loadChildren: () =>
          import("../pages/billpay/billpay.module").then(
            m => m.BillPayPageModule
          )
      },
      {
        path: "billreceive",
        loadChildren: () =>
          import("../pages/billreceive/billreceive.module").then(
            m => m.BillReceivePageModule
          )
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRouter {}
